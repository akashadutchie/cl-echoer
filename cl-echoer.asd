(defsystem #:cl-echoer
  :name "CL-ECHOER"
  :author "Akasha Peppermint"
  :version "3"
  :license "MIT"
  :depends-on (#:alexandria
               #:str
               #:cl-fad
               #:posix-shm
               #:if-letstar)
  :serial t
  :components ((:file "emitter")))

(defpackage #:cl-echoer
  (:use #:cl #:alexandria)
  (:export #:echo  
           #:create-echoer
           #:no-h-start ;synonym for above
           
           ;; NOTE kludges
           #:start           
           #:launch-window))
