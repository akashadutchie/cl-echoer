;; the buffer is the latest state\
(in-package :cl-echoer)

(defvar %newline-string (format nil "~a" #\newline))
(defvar *echoer-pprinter*
  (let* ((original-print-pprint-dispatch (copy-pprint-dispatch))
         (modified-print-pprint-dispatch (copy-pprint-dispatch)) 
         (*print-pprint-dispatch* modified-print-pprint-dispatch))
    (set-pprint-dispatch
     'float
     (lambda (s f)
       (if (sb-kernel:float-infinity-or-nan-p f)
           (let ((*print-pprint-dispatch* original-print-pprint-dispatch))
             (format s "~a" f))
           (format s "~,2f" f))))
    modified-print-pprint-dispatch))

(defun prepare-message (cats max-message-size msg &rest msgs)
  (let* ((*print-pprint-dispatch* *echoer-pprinter*)
         (e (apply #'format nil "(~a \"~@{~a~^ ~}\")" cats msg msgs))
         (f (str:shorten max-message-size e :ellipsis "\")"))
         (message f))
    (declare (dynamic-extent e f))
    message))

(defclass echoer ()
  ((id :initarg :id
       :reader echoer-id
       :initform (string (gensym)))
   (max-categories :initarg :max-categories
                   :reader max-categories
                   :initform 512)
   (max-message-size :initarg :max-message-size
                     :reader max-message-size
                     :initform 251)
   (total-message-size :initarg :total-message-size
                       :reader total-message-size
                       :initform 256)
   (cat-counter :initarg :cat-counter
                :reader cat-counter
                :initform 0)
   (cat-registry :initarg :cat-registry
                 :reader cat-registry
                 :initform nil)
   (shm :reader cat-shm
        :initform nil)
   (mmap :reader cat-mmap
         :initform nil) 
   (ch-shm :reader ch-cat-shm
           :initform nil)
   (ch-mmap :reader ch-cat-mmap
            :initform nil)))

;; categories to numbers
(defun initialise-echoer (echoer)
  (with-slots (id shm mmap ch-shm ch-mmap cat-counter cat-registry total-message-size max-categories) echoer
    (if shm (posix-shm:close-shm shm))
    (let ((size (* total-message-size max-categories (cffi:foreign-type-size :char))))
      (setq cat-registry (make-hash-table :test #'equalp)
            cat-counter 0
            shm (posix-shm:open-shm
                 (str:downcase (format nil "/~a" id)) :direction :io :if-does-not-exist :create
                 :permissions '(:user-read :user-write))
            mmap (posix-shm:mmap-shm shm (* max-categories total-message-size) :prot '(:write)))
      (posix-shm:truncate-shm shm size) 
      (setq ch-shm (posix-shm:open-shm
                    (str:downcase (format nil "/chr-~a" id)) :direction :io :if-does-not-exist :create
                    :permissions '(:user-read :user-write)))
      (posix-shm:truncate-shm ch-shm 128) 
      (setq ch-mmap (posix-shm:mmap-shm ch-shm 128 :prot '(:write))))
    echoer))

(defun create-echoer (id)
  (initialise-echoer (make-instance 'echoer :id id)))

(defun close-echoer (echoer)
  (with-slots (shm mmap ch-shm ch-mmap) echoer
    (when mmap
      (posix-shm:close-shm mmap)
      (posix-shm:close-shm ch-mmap)
      (setf mmap nil)
      (setf ch-mmap nil))))

(defun category-index (echoer &rest cats)
  (declare (dynamic-extent cats))
  (with-slots ( cat-counter cat-registry max-categories max-message-size) echoer
    (let ((mmap (slot-value echoer 'mmap)))
      (if-let ((id (gethash cats cat-registry)))
        id
        (when (< cat-counter max-categories)
          (setf (gethash (copy-seq cats) cat-registry)
                cat-counter 
                cat-counter (1+ cat-counter))
          (if (= cat-counter max-categories)
              (progn 
                (print "cl-echoer emitter: No more category space")
                (setf cat-counter (+ max-categories 1))
                nil)
              (- cat-counter 1)))))))

(defun get-categories (echoer)
  (with-slots (cat-registry) echoer
    (loop for k being the hash-keys of cat-registry collect k)))

(defun echo (echoer cats message-token &rest message-tokens)
  (with-slots (cat-counter cat-registry max-message-size total-message-size) echoer
    (let ((mmap (slot-value echoer 'mmap))
          (ch-mmap (slot-value echoer 'ch-mmap)))
      (if-letstar:when-let ((index (apply #'category-index echoer cats))
                            (message (apply #'prepare-message cats max-message-size message-token message-tokens)))
        (declare (dynamic-extent index message))
        (loop for i from (- total-message-size (length message)) below total-message-size
              do (setf (cffi:mem-aref mmap :char i)
                       ;; (coerce (char-int #\ ) '(unsigned-byte 8))
                       0))
        (loop for c across message
              for j from 0
              do (setf (cffi:mem-aref mmap :char (+ j (* index total-message-size)))
                       (coerce (char-int c) '(unsigned-byte 8))))
        
        (setf (cffi:mem-aref ch-mmap :int 0) 1)
        ))))

(defun ohce (echoer cats datum &rest arguments)
  (with-slots (cat-counter cat-registry max-message-size total-message-size) echoer
    (if-letstar:when-let ((mmap (slot-value echoer 'mmap))
                          (index (apply #'category-index echoer cats))
                          (message (apply #'prepare-message cats max-message-size datum arguments)))
      (declare (dynamic-extent index message))
      (loop for c across message
            for j from 0
            collect (code-char (cffi:mem-aref mmap :char (+ j (* index total-message-size))))))))

(defun clear-all (echoer)
  (with-slots (cat-registry) echoer
    (loop for k being the hash-keys of cat-registry
          do (echo echoer k ""))))

(defmethod print-object ((obj echoer) out)
  (with-slots () obj
    (print-unreadable-object (obj out :type t)
      (format out "~<~:_foo-slot = ~A~:>" (list foo-slot)))))


;; Convenience Kludges. 

(defun start (echoer-id)
  "one-function starter"
  (assert echoer-id)
  (let ((echoer (create-echoer echoer-id)))
    (launch-window echoer-id)
    echoer))

(defun no-h-start (echoer-id)
  "synonym for #'create-echoer"
  (assert echoer-id)
  (create-echoer echoer-id))

(defun launch-window (echoer)
  (assert (and echoer (not (equalp echoer ""))))
  (let* ((echoer-listener-d (format nil "~a" (asdf:system-relative-pathname :cl-echoer-listener "window.lisp")))
         (proc (uiop:launch-program "konsole --separate --hold --hide-menubar -e /bin/bash")))

    ;; 'it-works-on-my-system' hacks. optional. Portacle on linux launching konsole.
    (sleep .5)
    (ignore-errors
     (uiop:run-program (concatenate 'string "qdbus org.kde.konsole-"
                                    (write-to-string (1+ (uiop:process-info-pid proc)))
                                    " /Sessions/1 org.kde.konsole.Session.setProfile \"Profile 1\"")))
    (sleep .5) 
    (uiop:launch-program
     `("qdbus" ,(format nil "org.kde.konsole-~A" (1+ (uiop:process-info-pid proc)))
               "/Sessions/1" 
               "runCommand"
               ,(concatenate 'string
                             "sbcl --load " echoer-listener-d " " (str:downcase (format nil "~a" echoer))))
     :ignore-error-status t)
    proc))
