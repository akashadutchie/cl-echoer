;; (in-package :cl-echoer-listener)

(asdf:load-system :posix-shm)
(asdf:load-system :serapeum)

(defvar %newline-string (format nil "~a" #\newline))
(defvar *program-start* (serapeum:get-unix-time))
(defvar *echoer-pprinter*
  (let* ((original-print-pprint-dispatch (copy-pprint-dispatch))
         (modified-print-pprint-dispatch (copy-pprint-dispatch)) 
         (*print-pprint-dispatch* modified-print-pprint-dispatch))
    (set-pprint-dispatch
     'float
     (lambda (s f)
       (if (sb-kernel:float-infinity-or-nan-p f)
           (let ((*print-pprint-dispatch* original-print-pprint-dispatch))
             (format s "~a" f))
           (format s "~,2f" f))))
    modified-print-pprint-dispatch))

(defclass echoer ()
  ((id :initarg :id
       :reader echoer-id
       :initform (string (gensym)))
   (changed :reader echoer-canged
            :initform t)
   (max-categories :initarg :max-categories
                   :reader max-categories
                   :initform 512)
   (max-message-size :initarg :max-message-size
                     :reader max-message-size
                     :initform 253)
   (total-message-size :initarg :total-message-size
                       :reader total-message-size
                       :initform 256)
   (cat-counter :initarg :cat-counter
                :reader cat-counter
                :initform 0)
   (cat-registry :initarg :cat-registry
                 :reader cat-registry
                 :initform nil)
   (id->cat-registry :initarg :id->cat-registry
                     :reader id->cat-registry
                     :initform nil)
   (messages :initarg echoer-messages
             :reader echoer-messages
             :initform nil)
   (shm :reader cat-shm
        :initform nil)
   (mmap :reader cat-mmap
         :initform nil)

   
   (ch-shm :reader cat-ch-shm
           :initform nil)
   (ch-mmap :reader cat-ch-mmap
            :initform nil)

   (last-render :initform "")))

(defclass message ()
  ((since :reader message-since :initform 0)
   (changed :reader message-changed :initform t)
   (message :reader message-contents :initform nil)
   ;; could be useful.
   (last-message :reader message-contents :initform nil)))

;; categories to numbers
(defun initialise-listener (echoer)
  (with-slots (id shm mmap ch-shm ch-mmap cat-counter cat-registry id->cat-registry total-message-size max-categories messages) echoer
    (if shm (posix-shm:close-shm shm))
    (let ((size (* total-message-size max-categories (cffi:foreign-type-size :char))))
      (setq cat-registry (make-hash-table :test #'equalp)
            id->cat-registry (make-hash-table :test #'equalp) 
            messages (make-hash-table :test #'equalp) ;?
            cat-counter 0
            shm (posix-shm:open-shm (str:downcase (format nil "/~a" id)) :direction :io :if-does-not-exist :create
                                                                         :permissions '(:user-read :user-write))

            )
      (posix-shm:truncate-shm shm size) 
      (setq mmap (posix-shm:mmap-shm shm size :prot '(:read)))
      
      ;; (setq ch-shm (posix-shm:open-shm (str:downcase (format nil "/chr-~a" id)) :direction :io :if-does-not-exist :create
      ;;                                                                           :permissions '(:user-read :user-write)))
      ;; (posix-shm:truncate-shm ch-shm 128) 
      ;; (setq ch-mmap (posix-shm:mmap-shm ch-shm 128 :prot '(:read)))
      (setq ch-shm (posix-shm:open-shm
                    (str:downcase (format nil "/chr-~a" id)) :direction :io :if-does-not-exist :create
                    :permissions '(:user-read :user-write)))
      (posix-shm:truncate-shm ch-shm 128) 
      (setq ch-mmap (posix-shm:mmap-shm ch-shm 128 :prot '(:read)))
      echoer)))

(defun create-listener (id)
  (initialise-listener (make-instance 'echoer :id id)))

(defun close-listener (echoer)
  (with-slots (shm mmap) echoer
    (when mmap
      (posix-shm:close-shm mmap)
      (setf mmap nil))))

(defun category-index (echoer &rest cats)
  (declare (dynamic-extent cats))
  (with-slots (mmap cat-counter id->cat-registry cat-registry max-categories max-message-size) echoer
    (alexandria-1:if-let ((id (gethash cats cat-registry)))
      id
      (let ((cats (copy-seq cats)))
        (when (< cat-counter max-categories)
          (setf (gethash cats cat-registry)
                cat-counter 
                cat-counter (1+ cat-counter)
                (gethash cat-counter id->cat-registry) cats)
          
          (if (= cat-counter max-categories)
              (progn 
                (print "cl-echoer emitter: No more category space")
                (setf cat-counter (+ max-categories 1))
                nil)
              (- cat-counter 1)))))))

(defun get-categories (echoer)
  (with-slots (cat-registry) echoer
    (loop for k being the hash-keys of cat-registry collect k)))


(defun write-message (m)
  (list
   (coerce-string (slot-value m 'message))
   (coerce-string (- *program-start* (slot-value m 'since)))
   (str:shorten 12 (coerce-string (slot-value m 'last-message)))))

(defvar +CELL-FORMATS+ '(:left   "~vA"
                         :center "~v:@<~A~>"
                         :right  "~v@A"))

(defun coerce-string (data)
  (if (stringp data)
      data
      (format nil "~A" data)))

;; https://gist.github.com/WetHat/a49e6f2140b401a190d45d31e052af8f
(defun format-table (stream data
                     &key column-label
                       column-align)
  (let* ((col-count (length column-label))
         (strtable  (cons column-label  ; table header
                          ;; (loop for row in data ; table body with all cells as strings
                          ;;       collect (loop for cell in row
                          ;;                     collect (if (stringp cell)
                          ;;                                 cell
                          ;;               ;else
                          ;;                                 (format nil "~A" cell))))
                          (loop for c being the hash-keys of data
                                  using (hash-value m)
                                collect (apply 'list
                                               (coerce-string c)
                                               (write-message m)))))
         (col-widths (loop with widths = (make-array col-count :initial-element 0)
                           for row in strtable
                           do (loop for cell in row
                                    for i from 0
                                    do (setf (aref widths i)
                                             (max (aref widths i) (length cell))))
                           finally (return widths))))
                                        ;------------------------------------------------------------------------------------
                                        ; splice in the header separator
    (setq strtable
          (nconc (list (car strtable)                  ; table header
                       (loop for align in column-align ; generate separator
                             for width across col-widths
                             collect (case align
                                       (:left   (format nil ":~v@{~A~:*~}"
                                                        (1- width)  "-"))
                                       (:right  (format nil "~v@{~A~:*~}:"
                                                        (1- width)  "-"))
                                       (:center (format nil ":~v@{~A~:*~}:"
                                                        (- width 2) "-")))))
                 (cdr strtable)))       ; table body
                                        ;------------------------------------------------------------------------------------
                                        ; Generate the formatted table
    (let ((row-fmt (format nil "| ~{~A~^ | ~} |~~%" ; compile the row format
                           (loop for align in column-align
                                 collect (getf +CELL-FORMATS+ align))))
          (widths  (loop for w across col-widths collect w)))
                                        ; write each line to the given stream
      (dolist (row strtable)
        (apply #'format stream row-fmt (mapcan #'list widths row))))))

(defun tostring (echoer)
  (let ((messages (slot-value echoer 'messages)))
    (with-output-to-string (str)
      (format-table str messages
                    :column-label `("Category" ,(str:pad 70 "Message") ,(str:pad (length (format nil "~a" (serapeum:get-unix-time))) "Age")
                                               ,(str:shorten 12 "Last"))
                    :column-align '(:right :left :left :left)))))
(defun new-data-p (echoer)
  ;; (with-slots (ch-mmap) echoer
  ;;   (< 0 (cffi:mem-aref ch-mmap :int 0)))
  t
  )

(defun poll (echoer)
  (with-slots (id shm mmap ch-shm ch-mmap cat-counter cat-registry id->cat-registry messages total-message-size max-categories changed) echoer
    ;; (when (< 0 (cffi:mem-aref ch-mmap :int 0)))
    ;; (setf (cffi:mem-aref ch-mmap :int 0) 0)
    (loop
      for i from 0 repeat 511      
      for new-message-data = (map 'string 'code-char
                                  (loop for j from 0 repeat (- total-message-size 1)
                                        for char = (cffi:mem-aref mmap :char (+ j (* i total-message-size)))
                                        while (not (eq 0 char))
                                        collect char))
      do (alexandria:when-let* ((validp (and (str:starts-with? "((" new-message-data)
                                             (str:ends-with? ")" new-message-data)))
                                
                                ;; uhh This is a memory leak. should use a delim instead...
                                (new-message-cons  (read-from-string new-message-data))
                                (cats  (first new-message-cons))

                                ;; this isn't even a string
                                (new-message (str:join ", " (rest new-message-cons))))
           (let* ((message  (gethash cats messages))
                  (old-message  (if message
                                    (slot-value message 'message))))
             
             ;; do (setq j (+ 1 (mod j total-message-size)))
             ;; do(br new-message message)
             (if (not old-message)
                 (setf message (make-instance 'message)
                       (gethash cats messages) message
                       (gethash i id->cat-registry) cats
                       (gethash cats cat-registry) i))
             (with-slots (message       ; changed
                          since
                          last-message) message
               (setq last-message message
                     message new-message
                     changed (not (equalp old-message new-message))
                     since
                     ;; (if (since )
                     ;;     (- (serapeum:get-unix-time) since)
                     ;;     (serapeum:get-unix-time))
                     (cond (changed (serapeum:get-unix-time))
                           (t since))
                     cat-counter (max i cat-counter)))))         
      )))
