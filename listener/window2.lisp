(ql:quickload :trivial-signal)

(defun exit-on-signal (signo)
  (format *error-output* "~&received ~A~%" (signal-name signo))
  (sb-ext:exit :code 1 :abort t))

(setf (trivial-signal:signal-handler 2) #'exit-on-signal) ;; :term can also be :sigterm or 15

(ql:quickload :trivial-escapes)
(ql:quickload :alexandria)
(ql:quickload :serapeum)
(ql:quickload :cl-echoer-listener)

(print sb-ext:*posix-argv*)
(defvar *listener-name* "one")
(alexandria:if-let ((id (second sb-ext:*posix-argv*)))
  (setq *listener-name* id)
  (progn "need id as first argument" (exit :code 1)))
(named-readtables:in-readtable trivesc:readtable) 

(defvar *echoerr* (create-listener (intern *listener-name* :keyword)))
(format t #"~c\033]30;CL-ECHOER: ~a\007" #\ESC *listener-name*)

(defun clear-screen ()
  (format t "[H[2J[3J"))

(defvar *last-render* "")

(format t "[7l")

;; (sleep 1)
(loop
  ;; repeat 200
  with last = 0
  for i from 0
  for render = (progn
                 (poll *echoerr*) 
                 (tostring *echoerr*))
  do (setq *program-start* (serapeum:get-unix-time))
  if (or (< i 6) (> 0 last)
         (not (equalp render *last-render*))
         )
    do (clear-screen)
       (format t render)
       (terpri)
       (format t "~a" i) 
       (terpri)
       (force-output)
       (setq *last-render* render
             last 3)
  do (sleep 0.01))
(sleep 1)
(clear-screen)
(sb-ext:exit)
