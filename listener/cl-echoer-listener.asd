(defsystem #:cl-echoer-listener
  :name "CL-ECHOER listener"
  :author "Akasha Peppermint"
  :version "3"
  :license "MIT"
  :depends-on (#:alexandria
               #:serapeum
               #:str
               ;; #:bodge-nanovg 
               ;; #:bodge-nanovg/example
               ;; #:bodge-canvas
               #:posix-shm)
  :serial t
  :components ((:file "listener")
               ;; (:file "window")
               ))

(uiop:define-package #:cl-echoer-listener
  (:use #:cl #:alexandria)
  (:export
   #:poll
   #:create-listener
   cat-counter id->cat-registry messages)) 
